package go_calculator

import (
	"fmt"
	"strings"
)

func floatToStr(value float64) string {
	return fmt.Sprintf("%g", value)
}

func removeSpaces(expression string) string {
	return strings.ReplaceAll(expression, " ", "")
}