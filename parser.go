package go_calculator

import (
	"strconv"
)

type FloatOperand float64

func parseArgument(expression string) (FloatOperand, BinaryOperator) {
	number := ""

	for i := 0; i < len(expression); i++ {
		char := string(expression[i])

		_, err := strconv.ParseFloat(char, 0)
		if err != nil {
			if len(number) == 0 {
				return 0, parseOperator(char)
			}

			floatValue, _ := strconv.ParseFloat(number, 0)
			return FloatOperand(floatValue), nil

		} else {
			f, _ := strconv.ParseFloat(char, 0)
			number += floatToStr(f)
		}
	}

	floatValue, _ := strconv.ParseFloat(number, 0)
	return FloatOperand(floatValue), nil
}

func parseOperator(operator string) BinaryOperator {
	if operator == "+" {
		return new(AddOperator)
	} else if operator == "-" {
		return new(SubtractOperator)
	} else if operator == "*" {
		return new(MultiplyOperator)
	} else if operator == "/" {
		return new(DivisionOperator)
	} else if operator == "^" {
		return new(PowerOperator)
	}

	panic("unsupported operator '" + operator + "'")
}
