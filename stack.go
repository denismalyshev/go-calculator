package go_calculator

type OperatorStack []BinaryOperator

func (h OperatorStack) Len() int {
	return len(h)
}

func (h OperatorStack) Less(i, j int) bool {
	panic("not implemented")
}

func (h OperatorStack) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h *OperatorStack) Push(x interface{}) {
	*h = append(*h, x.(BinaryOperator))
}

func (h *OperatorStack) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

func (h *OperatorStack) Peek() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	return x
}

type OperandStack []float64

func (h OperandStack) Len() int {
	return len(h)
}

func (h OperandStack) Less(i, j int) bool {
	return h[i] < h[j]
}

func (h OperandStack) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h *OperandStack) Push(x interface{}) {
	*h = append(*h, x.(float64))
}

func (h *OperandStack) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}
