package go_calculator

type Calculator struct {
	operands  OperandStack
	operators OperatorStack
}

func (c *Calculator) Calculate(expression string) float64 {
	expression = removeSpaces(expression)

	c.operands = OperandStack{}
	c.operators = OperatorStack{}

	for len(expression) > 0 {
		operand, operator := parseArgument(expression)
		if operator != nil {
			c.pushOperator(operator)
			expression = expression[1:]
		} else {
			c.operands.Push(float64(operand))
			expression = expression[len(floatToStr(float64(operand))):]
		}
	}

	for c.operators.Len() > 0 {
		c.popOperator()
	}

	return c.operands.Pop().(float64)
}

func (c *Calculator) pushOperator(operator BinaryOperator) {
	if c.operators.Len() > 0 && c.operators.Peek().(BinaryOperator).priority() > operator.priority() {
		c.popOperator()
	}

	c.operators.Push(operator)
}

func (c *Calculator) popOperator() {
	rightOperand := c.operands.Pop().(float64)
	leftOperand := c.operands.Pop().(float64)

	result := c.operators.Pop().(BinaryOperator).execute(leftOperand, rightOperand)
	c.operands.Push(result)
}
