package go_calculator

import "math"

type BinaryOperatorPriority int

const (
	LOW BinaryOperatorPriority = iota
	MEDIUM
	HIGH
)

type BinaryOperator interface {
	execute(leftOperand, rightOperand float64) float64

	priority() BinaryOperatorPriority
}

type AddOperator struct{}

type SubtractOperator struct{}

func (SubtractOperator) execute(leftOperand, rightOperand float64) float64 {
	return leftOperand - rightOperand
}

func (SubtractOperator) priority() BinaryOperatorPriority {
	return LOW
}

func (AddOperator) execute(leftOperand, rightOperand float64) float64 {
	return leftOperand + rightOperand
}

func (AddOperator) priority() BinaryOperatorPriority {
	return LOW
}

type MultiplyOperator struct{}

func (MultiplyOperator) execute(leftOperand, rightOperand float64) float64 {
	return leftOperand * rightOperand
}

func (MultiplyOperator) priority() BinaryOperatorPriority {
	return MEDIUM
}

type DivisionOperator struct{}

func (DivisionOperator) execute(leftOperand, rightOperand float64) float64 {
	if rightOperand == 0 {
		panic("division by ZERO")
	}

	return leftOperand / rightOperand
}

func (DivisionOperator) priority() BinaryOperatorPriority {
	return MEDIUM
}

type PowerOperator struct{}

func (PowerOperator) execute(leftOperand, rightOperand float64) float64 {
	return math.Pow(leftOperand, rightOperand)
}

func (PowerOperator) priority() BinaryOperatorPriority {
	return HIGH
}
